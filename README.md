# Extraordinary-Queuing

The aim of this project was to help the Danish Road Directorate to automatically detect extraordinary queuing situations based on INRIX data. 

INRIX already provides a well-defined threshold based method for determining extraordinary queuing situations. However, it has several parameters that must be specified.

A new algorithm has been built to help the Danish Road Directorate determine these parameters automatically in a coherent and systematic way.
