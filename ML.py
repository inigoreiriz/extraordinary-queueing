

from __future__ import division
import os
import glob
import errno
import pandas as pd
import numpy as np
import re
import itertools
from datetime import datetime
from datetime import timedelta
from matplotlib.pyplot import *
import matplotlib.pyplot as plt
plt.style.use('ggplot')
from shuffle_data import * 

from sklearn import cross_validation
from sklearn.metrics import roc_curve, auc
from sklearn import cross_validation
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score


def build_lagged_time_series_feature_matrix(n_features):

    N = df.shape[0] #number of entries
    M = n_features
    X = np.zeros((N,M)) #matrix of features
    y = np.zeros((N,1)) #column matrix with EQ flags (ground truth)
    
    max_time_back_sec = 900
    total_lag_features = 15

    for row in range(N):

        fr = df_ts[df_ts['Segment ID'] == df['Segment Id'][row]]
        st_time = df['Time'][row] - timedelta(seconds = max_time_back_sec) #15 min back
        time_frame = fr[(fr['DateTime'] >= st_time) & (fr['DateTime'] < df['Time'][row])]
        time_frame = time_frame.drop_duplicates(subset = 'DateTime')

        if time_frame.shape[0] == total_lag_features:

            X[row,:] = np.mat(time_frame['Real Speed'].values[0:n_features]/df['FreeFlowSpeed'][row]) 
            y[row,:] = np.mat(df['Traffic Centre identification of EOQ'][row])

    return X, y

def crossval(X, y):
    
    #Initialize variables
    N = df.shape[0] #number of entries
    K = 10 # 10 fold cross validation

    f1_logreg_test, precision_logreg_test, recall_logreg_test = np.zeros(K), np.zeros(K), np.zeros(K)
    
    # Create 10 fold crossvalidation partition for evaluation
    CV = cross_validation.KFold(N, K, shuffle=True)

    k=0
    
    for train_index, test_index in CV:

        # extract training and test set for current CV fold
        X_train = X[train_index,:]
        y_train = y[train_index]
        X_test = X[test_index,:]
        y_test = y[test_index]

        ##### Fit and evaluate logistic Regression classifier
        log_classifier = lm.logistic.LogisticRegression()
        classifier = log_classifier.fit(X_train, y_train)

        y_est_train = classifier.predict(X_train) 
        y_est_test = classifier.predict(X_test)

        f1_train = f1_score(y_train, y_est_train) #compute train error
        f1_test = f1_score(y_test, y_est_test) #compute test error

        precision_train = precision_score(y_train, y_est_train)
        precision_test = precision_score(y_test, y_est_test)

        recall_train = recall_score(y_train, y_est_train)
        recall_test = recall_score(y_test, y_est_test)

        f1_vec_test[k], precision_vec_test[k], recall_vec_test[k] = f1_test, precision_test, recall_test     
        
        k+=1
        
    return (np.mean(f1_vec_test), np.mean(precision_vec_test), np.mean(recall_vec_test))



def plot_output_predictions_ahead_of_time(output):

    # Create directory where to store the curves
    dir_path = 'forecasting_eq_ahead_of_time/'
    os.makedirs(dir_path)

    fig = plt.figure(figsize=(15, 5))

    logreg, = plt.plot(output, color = '#DC143C', linewidth = 3 ,label = 'logistic regression')

    plt.ylim([0.5,1])
    plt.xlabel('T+k', fontsize=14)
    plt.ylabel('F1' fontsize=14)
    plt.legend(handles=[logreg])
    plt.legend(loc=4)

    save_path = dir_path + ylabel + '.png'
    fig.savefig(save_path)
     

def predict_eq_ahead_of_time():

    num_lags = 15

    curve_f1_logreg =  []
    curve_prec_logreg = []
    curve_recall_logreg = []

    for k in range(num_lags - 1):

        k += 1

        #update feature matrix
        X, y = build_lagged_time_series_feature_matrix(n_features = (num_lags - k))

        #perform cross-validation with a bunch of classifiers
        f1_logreg, prec_logreg, recall_logreg  = crossval(X,y)

        #save stuff for plotting later
        curve_f1_logreg.append(f1_logreg)
        curve_prec_logreg.append(prec_logreg)
        curve_recall_logreg.append(recall_logreg)

    plot_output_predictions_ahead_of_time(curve_f1_logreg)


def predict_eq_at_T0(df_additional_features):

    num_lags = 15

    # construct feature matrix with the list of features provided
    X, y = build_lagged_time_series_feature_matrix(n_features = num_lags)

    X_temp = pd.DataFrame(X)
    X_temp = pd.concat([X_temp, df_additional_features], axis=1)
    X = X_temp.as_matrix()

    #perform cross-validation with a bunch of classifiers
    f1_logreg, prec_logreg, recall_logreg  = crossval(X,y)


def roc_curve():

    ### ROC curve for logistic regression (best classifier)

    N = df.shape[0]
    K=10
    CV = cross_validation.KFold(N, K, shuffle=True)
    num_lags = 15

    X, y = build_lagged_time_series_feature_matrix(n_features = num_lags)

    y_true = []
    y_score = []

    for train_index, test_index in CV:

        # extract training and test set for current CV fold
        X_train = X[train_index,:]
        y_train = y[train_index]
        X_test = X[test_index,:]
        y_test = y[test_index]

        log_classifier = lm.logistic.LogisticRegression()
        trained_log_classifier = log_classifier.fit(X_train, y_train)

        #predict_proba returns the number of votes for each class
        log_classifier = log_classifier.fit(X_train, y_train)

        #print first which column refers to which class
        #print log_classifier.classes_ 

        proba_negatice_class = log_classifier.predict_proba(X_test)[:,0] # for calculating the probability of the negative clas
        proba_positive_class = log_classifier.predict_proba(X_test)[:,1] # for calculating the probability of the positive class

        #Make sure the class with highest probability is the one that is predicted
        #y_est_test = log_classifier.predict(X_test)
        #print prob_1st_class[0], prob_2nd_class[0]
        #print y_est_test[0]

        y_true.extend(y_test)
        y_score.extend(proba_positive_class)

    #y_true: true binary labels.
    #y_score: probability estimates of the positive class
    #example. y_true: spam/not spam as label of emails. y_score: 0.78 probability a given email is spam.
    #tpr example: probability of test being positive given that the disease is present.
    #pfr example: probability of test being negative given that the disease is not present.
    fpr, tpr, thresholds = roc_curve(y_true, y_score)
    roc_auc = auc(fpr, tpr)

    fig = plt.figure(figsize=(10, 5))

    roc, = plt.plot(fpr, tpr, label='ROC curve (area = %0.2f)' % (roc_auc))
    diagonal, = plt.plot([0, 1], [0, 1], linestyle='--', color='k', label = 'Random guess')
    plt.legend(handles=[roc, diagonal])
    plt.legend(loc=4)
    plt.xlabel('false positive rate')
    plt.ylabel('true positive rate')
    plt.title('ROC curve for logistic regression')
    fig.savefig('forecasting_eq_at_T0/ROC_curve')


if __name__ == "__main__":

    predict_eq_ahead_of_time()

    ### Add additional features to the feature matrix
    df_additional_features = pd.DataFrame()
    df_additional_features['Segment length'] = df['Segment length Km'].values #segment length
    weekdays = [date.weekday() for date in df['Time']] #convert date into a weekday (0-6)
    df_additional_features['Weekday Id'] = weekdays
    df_additional_features['Weekend Id'] = [0 if weekday < 5 else 1 for weekday in weekdays] #classify date into week day (0) or weekend (1)

    predict_eq_at_T0(df_additional_features)

    roc_curve()
