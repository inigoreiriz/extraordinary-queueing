
import os
import errno
import pandas as pd
import numpy as np
import sklearn
from sklearn import cross_validation
from sklearn.metrics import f1_score
from datetime import datetime
from datetime import timedelta
import warnings
import shutil
warnings.filterwarnings('ignore')
from checkEQ import *


def getUniqueCutoffCombinations(items, n):

    if n==0: yield []
    else:
        for i in xrange(len(items)):
            for cc in getUniqueCutoffCombinations(items[i+1:],n-1):
                yield [items[i]]+cc

def splitDataIntoClusters(data, cutoffs):

    n_clusters = len(cutoffs)
    splits = []

    for cluster in xrange(n_clusters+1):
        splits.append([])

    for ind, value in enumerate(data['Segment length Km'].values):
        assigned = False
        for cluster in xrange(n_clusters):
            if value < cutoffs[cluster]:
                splits[cluster].append(ind)
                assigned = True
                break
        if not assigned:
            splits[n_clusters].append(ind)

    return splits

def createDir(path):
    try: os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST: raise

# ** Optimizing f1 with cross-validation...**
def exploreCutoffs(thr_ran, dur_ran, can_ran, df, df_ts, cutoff_list, num_clusters, temp_path):

    #if temp folder exists, remove it
    if os.path.exists(temp_path):
        shutil.rmtree(temp_path)

    #number of samples in the data
    N = df.shape[0]

    # Create crossvalidation partition for evaluation, 10 fold
    K = 10
    CV = cross_validation.KFold(N,K,shuffle=True)

    #initialize counter
    f=0

    print('__Explore Cutoffs__')

    for train_index, test_index in CV:

        print('Computing CV fold: {0}/{1}..'.format(f+1,K))

        # extract training and test set for current CV fold
        X_train = df.loc[train_index]
        X_test = df.loc[test_index]

        # set dataframes' index from 0 to num samples
        X_train = X_train.set_index(np.arange(0,X_train.shape[0],1))
        X_test = X_test.set_index(np.arange(0,X_test.shape[0],1))

        combinations = [cutoff for cutoff in getUniqueCutoffCombinations(cutoff_list, num_clusters-1)]

        for cutoffs in combinations:

            trainIndices = splitDataIntoClusters(X_train, cutoffs)
            testIndices = splitDataIntoClusters(X_test, cutoffs)

            #initialize counter
            count = 0

            # Create arrays for storing training and testing f1s
            f1_test_vec = np.zeros(num_clusters)

            while (count < num_clusters):

                # get train subset according to that cluster
                X_train_clus = X_train.loc[trainIndices[count]]
                X_train_clus = X_train_clus.set_index(np.arange(0,X_train_clus.shape[0],1))

                # get test subset according to that cluster
                X_test_clus = X_test.loc[testIndices[count]]
                X_test_clus = X_test_clus.set_index(np.arange(0,X_test_clus.shape[0],1))

                # initialize 3 dimensional matrix for storing f1s
                f1_mat_train = np.zeros((len(thr_ran), len(dur_ran), len(can_ran)))

                # train model (optimize f1 for training set)
                for i,thr in enumerate(thr_ran):
                    for j,dur in enumerate(dur_ran):
                        for k,can in enumerate(can_ran):

                            #input arguments: thr, dur, can, data set with training samples, time series dataframe
                            f1_mat_train[i,j,k] = getF1(thr, dur, can, X_train_clus, df_ts)

                # extract thr, dur and can for maximum f1 in the matrix
                zipped_indices = np.where(f1_mat_train == f1_mat_train.max())
                indices = [index for index in zip(*zipped_indices)]

                thr_train = thr_ran[indices[0][0]]
                dur_train = dur_ran[indices[0][1]]
                can_train = can_ran[indices[0][2]]

                # compute test accuracy
                if not X_test_clus.empty:

                    # append optimized parameters in the test set
                    X_test_clus['Threshold'] = thr_train
                    X_test_clus['Duration'] = dur_train
                    X_test_clus['Cancellation Durarion'] = can_train

                    #compute f1
                    f1_test = f1_score(X_test_clus['Traffic Centre identification of EOQ'].values, predictEOQ(thr_train, dur_train, can_train, X_test_clus, df_ts))

                else:
                    f1_test = 0

                # store f1 test for each of the clusters
                f1_test_vec[count] = f1_test
                count += 1

            # Compute mean f1s for test set al
            mean_f1 = np.mean(f1_test_vec)

            # Save all the stuff to files
            strCutoffs = "_".join(str(cutoff) for cutoff in cutoffs)
            path = temp_path + 'cutoffsPerFold\\fold_' + str(f+1) + '\\' + 'cutoffs_' + strCutoffs + '\\'
            createDir(path)
            filepath = path + 'f1.txt'
            fp = open(filepath, 'w')
            fp.write(str(mean_f1))
            fp.close()

        f += 1
