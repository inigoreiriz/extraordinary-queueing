
import os
import errno
import pandas as pd
import numpy as np
import sklearn
import glob
from sklearn import cross_validation
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
import shutil
import warnings
warnings.filterwarnings('ignore')
from checkEQ import *
from exploreCutoffs import *
from pandas import ExcelWriter

def getClusterParameters(thr_ran, dur_ran, can_ran, df, df_ts, cutoffs, num_clusters, outputPath):

    if os.path.exists(outputPath):
        shutil.rmtree(outputPath)
    os.makedirs(outputPath)

    clusterIndices = splitDataIntoClusters(df, cutoffs)

    #initialize counter
    count = 0

    while (count < num_clusters):

        # get subset according to that cluster
        df_clus = df.loc[clusterIndices[count]]
        df_clus = df_clus.set_index(np.arange(0,df_clus.shape[0],1))

        # initialize 3 dimensional matrix for storing f1s
        f1_mat = np.zeros((len(thr_ran), len(dur_ran), len(can_ran)))

        # optimize f1
        for i,thr in enumerate(thr_ran):
            for j,dur in enumerate(dur_ran):
                    for k,can in enumerate(can_ran):

                            f1_mat[i,j,k] = getF1(thr, dur, can, df_clus, df_ts)

        # extract max_f1 (acc) and its values from 3-d matrix
        zipped_indices = np.where(f1_mat == f1_mat.max())

        multipleSetParams = []
        for indice in zip(*zipped_indices):
            multipleSetParams.append((thr_ran[indice[0]], dur_ran[indice[1]], can_ran[indice[2]]))

        f1s = []
        precisions = []
        recalls = []
        TNs = []
        FPs = []
        FNs = []
        TPs = []
        THRs = []
        DURs = []
        CANs = []

        for setParams in multipleSetParams:

            thr, dur, can = setParams[0], setParams[1], setParams[2]

            # set optimized parameters for computing f1s, precisions and recalls
            df_clus['Threshold'] = thr
            df_clus['Duration'] = dur
            df_clus['Cancellation Durarion'] = can

            #compute y_true, y_pred
            y_true = df_clus['Traffic Centre identification of EOQ'].values
            y_pred = predictEOQ(thr, dur, can, df_clus, df_ts)

            #compute f1,precision,recall and confusion matrix
            f1 = f1_score(y_true, y_pred)
            precision = precision_score(y_true, y_pred)
            recall = recall_score(y_true, y_pred)
            conf_mat = confusion_matrix(y_true, y_pred)
            TN = conf_mat.item((0,0))
            FP = conf_mat.item((0,1))
            FN = conf_mat.item((1,0))
            TP = conf_mat.item((1,1))

            f1s.append(f1)
            precisions.append(precision)
            recalls.append(recall)
            TNs.append(TN)
            FPs.append(FP)
            FNs.append(FN)
            TPs.append(TP)
            THRs.append(thr)
            DURs.append(dur)
            CANs.append(can)

        #create path for storing results
        path = outputPath + 'Cluster ' + str(count+1) + '\\'
        createDir(path)

        dc = pd.DataFrame()

        dc['F1'] = f1s
        dc['Precision'] = precisions
        dc['Recall'] = recalls
        dc['TN'] = TNs
        dc['FP'] = FPs
        dc['FN'] = FNs
        dc['TP'] = TPs
        dc['Threshold'] = THRs
        dc['Duration'] = DURs
        dc['CancDuration']  = CANs

        writer = ExcelWriter(path + 'results.xlsx')
        dc.to_excel(writer)
        writer.save()

        count += 1

    #compute global metrics
    outfolders = glob.glob(outputPath + '*')

    for num_clus in range(num_clusters):

        dc = pd.read_excel(outfolders[num_clus] + '\\results.xlsx')

        #replace optimized parameters in data set
        df.set_value(clusterIndices[num_clus],'Threshold', dc.Threshold.values[0])
        df.set_value(clusterIndices[num_clus],'Duration', dc.Duration.values[0])
        df.set_value(clusterIndices[num_clus],'Cancellation Durarion', dc.CancDuration.values[0])

    #compute y_true, y_pred
    y_true = df['Traffic Centre identification of EOQ'].values
    y_pred = []

    for row in range(df.shape[0]):

        #get segment ID
        segmentID = df['Segment Id'].loc[row]
        currentTime = df['Time'].loc[row]
        startTime =  currentTime - timedelta(seconds = 900)

        #filter by segmentID
        dfID= df_ts[df_ts['Segment ID'] == segmentID]
        #select frame: from currentTime to 15 min back
        df_TimeFrame = dfID[(dfID['DateTime'] >= startTime) & (dfID['DateTime'] <= currentTime)]
        #drop duplicates in order to have a dataframe of length 15
        df_TimeFrame = df_TimeFrame.drop_duplicates(subset = 'DateTime')
        #set row index from 0 to 15
        df_TimeFrame = df_TimeFrame.set_index(np.arange(0,df_TimeFrame.shape[0],1))
        #check if EQ
        isEQ = checkEOQ(df.Threshold.loc[row], df.Duration.loc[row], df['Cancellation Durarion'].loc[row], df_TimeFrame)

        y_pred.append(isEQ)

    #compute metrics and save them
    f1 = f1_score(y_true, y_pred)
    precision = precision_score(y_true, y_pred)
    recall = recall_score(y_true, y_pred)
    conf_mat = confusion_matrix(y_true, y_pred)
    TN = conf_mat.item((0,0))
    FP = conf_mat.item((0,1))
    FN = conf_mat.item((1,0))
    TP = conf_mat.item((1,1))

    dfinRes = pd.DataFrame()

    dfinRes['F1'] = [f1]
    dfinRes['Precision'] = [precision]
    dfinRes['Recall'] = [recall]
    dfinRes['TN'] = [TN]
    dfinRes['FP'] = [FP]
    dfinRes['FN'] = [FN]
    dfinRes['TP'] = [TP]

    dc = pd.DataFrame()
    dc['Cutoffs'] = cutoffs

    writer2 = ExcelWriter(outputPath + 'metrics.xlsx')
    dfinRes.to_excel(writer2)
    writer2.save()

    writer3 = ExcelWriter(outputPath + 'cutoffs.xlsx')
    dc.to_excel(writer3)
    writer3.save()
