
from __future__ import division
import pandas as pd
import numpy as np
from datetime import datetime
import glob

def strToBool(string):
    if string == 'TRUE': return True
    else: return False

def flipGroundTruth(df):

    list_of_files = glob.glob("CasesToFlip/*")

    for file in list_of_files:

        f = file.split('|')
        segment_id = str((f[0].split('/')[1]).strip())
        date = datetime.strptime(f[1].strip(),'%Y-%m-%d %H:%M:%S')

        match = df[(df['Segment Id'] == segment_id) & (df['Time'] == date)]

        if match['Traffic Centre identification of EOQ'].values[0] == True: df['Traffic Centre identification of EOQ'].loc[match.index[0]] = False
        else: df['Traffic Centre identification of EOQ'].loc[match.index[0]] = True

    return df

def cleanData(file1, file2, flipflags):

    #read extraordinary queueing file and skip 1st row (not useful)
    df =  pd.read_excel(file1, skiprows = 1)

    #remove 2nd row, not useful either
    df = df.drop(df.index[[0]])

    #drop last column with comments. Not useful either
    df = df.drop(df.columns[13], axis=1)

    df['Traffic Centre identification of EOQ'] = [strToBool(df['Traffic Centre identification of EOQ'].values[i]) for i in range(df.shape[0])]
    df['extraOrdinaryQueue INRIX'] = [strToBool(df['extraOrdinaryQueue INRIX'].values[i]) for i in range(df.shape[0])]

    #compute threshold as percentage
    df['Threshold'] = df['Threshold'].values / 100

    #remove rows with segment length below 320m
    df = df[df['Segment length Km'] > 0.320]

    #set index from 0 to num_rows
    df.index = np.arange(0, len(df))

    #drop set of columns that are not useful
    df = df.drop(df.columns[[0,5,8,9,10]], axis=1)

    # read time series file
    df_ts =  pd.read_excel(file2)

    #change column names
    df_ts.columns = ['Extract ID','Segment ID', 'DateTime', 'Travel Time (sec)', 'Normal Travel Time (sec)'
                                ,'Road Blocked', 'EQQ', 'Score', 'Reliability', 'Traffic Status'
                                ,'Forecast 15 min sec', 'Forecast 30 min sec', 'Free Flow Travel Time (sec)'
                                ,'Segment Length', 'Real Speed', 'Normal Speed', 'FreeFlowSpeed']

    #drop columns that are not useful
    df_ts = df_ts.drop(df_ts.columns[[0,3,4,5,6,7,8,9,10,11,12]], axis=1)

    #compute segment length in km
    df_ts['Segment Length'] = df_ts['Segment Length'].values / 1000

    #convert date in string format to a datetime object
    df_ts['DateTime'] = [datetime.strptime(df_ts.DateTime[i], '%d.%m.%Y %H:%M') for i in range(df_ts.shape[0])]

    #flip incorrect flags in the groundtruth
    if flipflags == 1:
        df = flipGroundTruth(df)

    #compute matches between 2 files in order to remove those samples
    #that do not contain a match in the time series file

    not_matches = []
    for row in range(df.shape[0]):

        segmentID = df['Segment Id'].loc[row]
        currentTime = df['Time'].loc[row]

        dfID = df_ts[df_ts['Segment ID'] == segmentID]
        dT= dfID[dfID['DateTime'] == currentTime]

        if dT.shape[0] == 0: not_matches.append(row)

    if not_matches:
        #drop rows that do not have a match
        df = df.drop(df.index[[not_matches]])
        df = df.set_index(np.arange(0,df.shape[0],1))

    return df, df_ts
