
import pandas as pd
import numpy as np
import sklearn
from sklearn.metrics import f1_score
from datetime import datetime
from datetime import timedelta

'''
Threshold: the percentage that indicates if extraordinary queueing is present
		   and the flag should be set.
Duration: The amount of time the percentage needs to be met before the flag is set.
Cancelation duration: The amount of time the percentage needs to be unmet before the
					  EQ alert is removed.

'''
def checkEOQ(threshold, duration_parameter, cancelation_duration, df_TimeFrame):

	numConsecutiveAboveThreshold = 0
	numConsecutiveBelowThreshold = 0

	#Reverse dataframe. We need to start sweeping from the currentTime
	df_TimeFrame = df_TimeFrame.iloc[::-1]
	df_TimeFrame = df_TimeFrame.set_index(np.arange(0,df_TimeFrame.shape[0],1))

	for i in range(df_TimeFrame.shape[0]):

		if df_TimeFrame['Real Speed'].loc[i] >= (threshold * df_TimeFrame['Normal Speed'].loc[i]):
			# it is above
			numConsecutiveAboveThreshold += 60
			numConsecutiveBelowThreshold = 0
		else:
			# it is below
			numConsecutiveAboveThreshold = 0
			numConsecutiveBelowThreshold += 60

		if numConsecutiveAboveThreshold > cancelation_duration:
			return False
		if numConsecutiveBelowThreshold >= duration_parameter:
			return True

	return False

def getF1(threshold, duration_parameter, cancelation_duration, df_Queue, df_TimeSeries):

	y_true = []
	y_pred = []

	#loop through all samples in extraordinary queueing file
	for row in range(df_Queue.shape[0]):

		#get segment ID
		segmentID = df_Queue['Segment Id'].loc[row]
		currentTime = df_Queue['Time'].loc[row]
		startTime =  currentTime - timedelta(seconds = 900)

		##filter time series file to match (segmentID, timeframe) pair
		#filter by segmentID
		dfID= df_TimeSeries[df_TimeSeries['Segment ID'] == segmentID]
		#select frame: from currentTime to 15 min back
		df_TimeFrame = dfID[(dfID['DateTime'] >= startTime) & (dfID['DateTime'] <= currentTime)]
		#drop duplicates in order to have a dataframe of length 15
		df_TimeFrame = df_TimeFrame.drop_duplicates(subset = 'DateTime')
		#set row index from 0 to 15
		df_TimeFrame = df_TimeFrame.set_index(np.arange(0,df_TimeFrame.shape[0],1))

		isEQ = checkEOQ(threshold, duration_parameter, cancelation_duration, df_TimeFrame)

		y_pred.append(isEQ)
		#append groundTruth
		y_true.append(df_Queue['Traffic Centre identification of EOQ'].loc[row])


	f1 = f1_score(y_true, y_pred)

	return f1

def predictEOQ(thrTrain, durTrain, canTrain, df_Queue, df_TimeSeries):

	y_pred = []

		#loop through all samples in extraordinary queueing file
	for row in range(df_Queue.shape[0]):

		#get segment ID
		segmentID = df_Queue['Segment Id'].loc[row]
		currentTime = df_Queue['Time'].loc[row]
		startTime =  currentTime - timedelta(seconds = 900)

		##filter time series file to match (segmentID, timeframe) pair
		#filter by segmentID
		dfID= df_TimeSeries[df_TimeSeries['Segment ID'] == segmentID]
		#select frame: from currentTime to 15 min back
		df_TimeFrame = dfID[(dfID['DateTime'] >= startTime) & (dfID['DateTime'] <= currentTime)]
		#drop duplicates in order to have a dataframe of length 15
		df_TimeFrame = df_TimeFrame.drop_duplicates(subset = 'DateTime')
		#set row index from 0 to 15
		df_TimeFrame = df_TimeFrame.set_index(np.arange(0,df_TimeFrame.shape[0],1))

		isEQ = checkEOQ(thrTrain, durTrain, canTrain, df_TimeFrame)

		y_pred.append(isEQ)

	return y_pred
