
import glob
import re
import os
import numpy as np
import shutil

def getBestCutoffs(temp_path):

	out_path = temp_path + 'CutoffAccuracies\\'
	if os.path.exists(out_path):
		shutil.rmtree(out_path)
	os.makedirs(out_path)

	path = temp_path + 'cutoffsPerFold\\*'

	for fold in glob.glob(path):
		folds = fold + '\\*'
		for cutoff in glob.glob(folds):
			cutoffs = cutoff + '\\*'
			for file in glob.glob(cutoffs):
				match = re.search('f1.txt', file)
				if match:
					fw = open(out_path + file.split('\\')[3] + '.txt', 'a')
					fr = open(file, 'r')
					f1 = fr.read()
					fw.write(f1)
					fw.write('\n')
					fw.close()
					fr.close()

	#compute mean f1 for each pair of cutoffs
	max_score = 0
	for file in glob.glob(temp_path + 'CutoffAccuracies\\*'):

		fr = open(file, 'r')
		scores = fr.read().splitlines()
		scores = [float(score) for score in scores[1:]]

		#get max f1 score together with cutoff
		if np.mean(scores) > max_score:
			max_score = np.mean(scores)
			best_cutoff = os.path.basename(file)

	best_cutoff = os.path.splitext(best_cutoff)[0]
	best_cutoffs = [float(cutoff) for cutoff in best_cutoff.split('_')[1:]]

	return best_cutoffs
