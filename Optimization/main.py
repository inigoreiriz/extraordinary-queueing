
import numpy as np
from loadData import *
from exploreCutoffs import *
from checkEQ import *
from extractBestCutoff import *
from findParameters import *
from plotting import *
from parseconfig import *

def main():

    #parse configuration file with input parameters
    params = parser(fname = 'configuration file')
    
    #default step for duration and cancelation duration
    def_step = 60

    #threshold, duration and cancelation duration ranges to explore
    thr_ran = np.arange(params['MIN_THRESHOLD'], params['MAX_THRESHOLD'], params['STEP_THRESHOLD']) 
    dur_ran = np.arange(params['MIN_DURATION'], params['MAX_DURATION'], def_step) 
    can_ran = np.arange(params['MIN_CANCELATION_DURATION'], params['MAX_CANCELATION_DURATION'], def_step)

    df, df_ts = cleanData(params['PATH_EQ_FILE'], params['PATH_TIMESERIES_FILE'], params['FLIPFLAGS'])
    #plotInitialTimeSeries(df, df_ts)
    exploreCutoffs(thr_ran, dur_ran, can_ran, df, df_ts, params['CUTOFFS'], params['NUM_CLUSTERS'], params['TEMP_PATH'])
    
    cutoffs = getBestCutoffs(params['TEMP_PATH'])
    getClusterParameters(thr_ran, dur_ran, can_ran, df, df_ts, cutoffs, params['NUM_CLUSTERS'], params['OUTPUT_PATH'])

if __name__ == "__main__":
	main()
