
def parser(fname):

	params = {}

	with open(fname) as f:
		content = f.readlines()

	content = [str(line.split('=')[1]) for line in content]
	#remove characters like white spaces and "\n" at the end of each line
	content = [x.strip() for x in content]
	#parse line with cutoffs
	cutoffs = [float(ele) for ele in content[12].split(',')]

	params['PATH_EQ_FILE'] = content[0]
	params['PATH_TIMESERIES_FILE'] = content[1]
	params['MIN_THRESHOLD'] = float(content[2])
	params['MAX_THRESHOLD'] = float(content[3])
	params['STEP_THRESHOLD'] = float(content[4])
	params['MIN_DURATION'] = float(content[5])
	params['MAX_DURATION'] = float(content[6])
	params['MIN_CANCELATION_DURATION'] = float(content[7])
	params['MAX_CANCELATION_DURATION'] = float(content[8])
	params['OUTPUT_PATH'] = content[9]
	params['TEMP_PATH'] = content[10]
	params['NUM_CLUSTERS'] = int(content[11])
	params['CUTOFFS'] = cutoffs
	params['FLIPFLAGS'] = int(content[13])

	return params
