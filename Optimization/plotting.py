
import os
import pandas as pd
import numpy as np
from datetime import datetime
from datetime import timedelta
from matplotlib.pyplot import *
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from checkEQ import *
       
def plotInitialTimeSeries(df, df_ts):

    pathPlots = 'CasesToFlip/'
    os.makedirs(pathPlots)

    for row in range(df.shape[0]):

        thr = df['Threshold'].loc[row]
        duration = df['Duration'].loc[row]
        canDuration = df['Cancellation Durarion'].loc[row]

        segmentID = df['Segment Id'].loc[row]
        currentTime = df['Time'].loc[row]
        startTime =  currentTime - timedelta(seconds = 900)
        endTime = currentTime + timedelta(seconds = 900)

        dfID= df_ts[df_ts['Segment ID'] == segmentID]
        #extract time series from (-15min to +15min) for plotting
        df_TimeFrame = dfID[(dfID['DateTime'] >= startTime) & (dfID['DateTime'] <= endTime)]
        df_TimeFrame = df_TimeFrame.drop_duplicates(subset = 'DateTime')
        df_TimeFrame = df_TimeFrame.set_index(np.arange(0,df_TimeFrame.shape[0],1))

        baseline = thr * df_TimeFrame['Normal Speed'].values

        BS = pd.Series(baseline, index=df_TimeFrame['DateTime'])
        TS = pd.Series(list(df_TimeFrame['Real Speed']) , index = df_TimeFrame['DateTime'])

        fig = plt.figure(figsize=(15, 5))
        
        BS.plot(linewidth = 2, color='blue')
        TS.plot(linewidth = 2, color='green')

        lineObservation = plt.axvline(x=currentTime, color='red', linewidth = 4, label = 'Observation')

        lineDuration = plt.axvline(x=(currentTime - timedelta(seconds=duration)), 
                                    color='purple', ls='dashed', linewidth=4, label = 'Non-optimized duration') 

        linecanDuration = plt.axvline(x=(currentTime - timedelta(seconds=canDuration)), 
                                    color='black', ls='dashed', linewidth=4, label = 'Non-optimized cancelation') 

        plt.legend(handles=[lineObservation, lineDuration, linecanDuration])
                        
        plt.xlabel('Time')
        plt.ylabel('Segment Speed')
         
        flagDRD = df['Traffic Centre identification of EOQ'].loc[row]
        flagINRIX = df['extraOrdinaryQueue INRIX'].loc[row]

        #in order our algorithm to check if it is extraordinary queueing we do not need the time series from
        #(-15min to + 15min), but from (-15min to currentTime)
        df_TimeFrame = dfID[(dfID['DateTime'] >= startTime) & (dfID['DateTime'] <= currentTime)]
        df_TimeFrame = df_TimeFrame.drop_duplicates(subset = 'DateTime')
        df_TimeFrame = df_TimeFrame.set_index(np.arange(0,df_TimeFrame.shape[0],1))

        #flag computed by our algorithm
        flagALGO = checkEOQ(thr, duration, canDuration, df_TimeFrame)

        if flagALGO != flagDRD:

            filename = (pathPlots + str(segmentID) + " | " + str(currentTime) + " | DRD:" + str(flagDRD) 
                            +  " | INRIX: " + str(flagINRIX) + "| ALGO: " + str(flagALGO))

            fig.savefig(filename)

